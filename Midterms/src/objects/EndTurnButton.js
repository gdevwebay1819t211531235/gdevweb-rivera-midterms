import { Container, Graphics, utils, Sprite } from "pixi.js";

class EndTurnButton extends Container{

    constructor(onClickEvent)
    {
        super();

        this.interactive = true;
        this.onClickEvent = onClickEvent

        var cache = utils.TextureCache;
        this.endTurnButton = new Sprite(cache["assets/images/next.png"]);
        this.endTurnButton.scale.x = 0.2;
        this.endTurnButton.scale.y = 0.2;
        this.endTurnButton.x = 870;
        this.endTurnButton.y = 600;
        this.addChild(this.endTurnButton);
    }

    mousedown(e)
    {
        this.onClickEvent();
    }
}
export default EndTurnButton;