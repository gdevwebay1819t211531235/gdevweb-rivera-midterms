import { Container, Graphics, utils, Sprite } from "pixi.js";

class SpawnPicture extends Container{

    constructor(condition)
    {
        super();

        var cache = utils.TextureCache;
        if (condition == 1 )
        {
            this.newPicture = new Sprite(cache["assets/images/win.png"]);
            this.newPicture.scale.x = 5;
            this.newPicture.scale.y = 5;
        }
        if (condition == 2 )
        {
            this.newPicture = new Sprite(cache["assets/images/lose.png"])
            this.newPicture.scale.x = 3;
            this.newPicture.scale.y = 3;
        };
        
        this.newPicture.x = 0;
        this.newPicture.y = 6;
        this.addChild(this.newPicture);
    }
}
export default SpawnPicture;