import { Container, Graphics, utils, Sprite } from "pixi.js";

class UpdateStuffDisplay extends Container{

    constructor(condition, xPos, yPos)
    {
        super();

        var cache = utils.TextureCache;
        if (condition == 1 )
        {
            this.newPicture = new Sprite(cache["assets/images/w.jpg"]);
            this.newPicture.scale.x = 1;
            this.newPicture.scale.y = 1;
        }
        if (condition == 2 )
        {
            this.newPicture = new Sprite(cache["assets/images/b.jpg"])
            this.newPicture.scale.x = 1;
            this.newPicture.scale.y = 1;
        };
        if (condition == 3 )
        {
            this.newPicture = new Sprite(cache["assets/images/x.jpg"])
            this.newPicture.scale.x = 1;
            this.newPicture.scale.y = 1;
        };
        
        this.newPicture.x = xPos;
        this.newPicture.y = yPos;

        this.addChild(this.newPicture);
    }
}
export default UpdateStuffDisplay;