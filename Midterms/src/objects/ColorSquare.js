import { Container, Graphics } from "pixi.js";

class ColorSquare extends Container{

    constructor(colorCode, colorName, onClickEvent, xPosition, yPosition)
    {
        super();

        this.interactive = true;

        this.colorCode = colorCode;
        this.colorName = colorName;

        var box = new Graphics();
        box.beginFill(this.colorCode);
        box.drawRect(xPosition,yPosition,50,50);
        box.endFill();
        this.addChild(box);
        this.onClickEvent = onClickEvent
        this.color;
    }

    mousedown(e)
    {
        this.onClickEvent(this.colorName, this.colorCode);
    }
}
export default ColorSquare;