import { Container, utils, Sprite, Graphics, Text } from "pixi.js";
import ColorSquare from "../objects/ColorSquare";
import EndTurnButton from "../objects/EndTurnButton";
import SpawnPicture from "../objects/SpawnPicture";
import UpdateStuffDisplay from "../objects/UpdateStuffDisplay";

class TitleScreen extends Container
{
    constructor()
    {
        super();

        this.interactive = true;

        var bgGraphics = new Graphics();
        bgGraphics.beginFill(0x808080);
        bgGraphics.drawRect(0,0,1900,1000);
        bgGraphics.endFill();
        this.addChild(bgGraphics);

        var titleText = new Text('GDEVWEB MIDTERMS');
        titleText.x = 800;
        titleText.y = 50;

        this.addChild(titleText);
        

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var colorCodes = ['0x0000FF', '0xFF0000', '0xFFFF00', '0x00FF00', '0x00FFFF', '0xFF00FF', '0xFFA500', '0xFF1493'];
        var colors = ['blue', 'red', 'yellow', 'green', 'cyan', 'magenta', 'orange', 'pink'];


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.randomColors = [];
        this.GenerateRandomColors(colors);
        this.chosenColors = [];
        this.turn = 1;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var posIncrement = 0;
        for (let index = 0; index < colors.length; index++) 
        {
            var colorSqr = new ColorSquare(colorCodes[index], colors[index] , this.OnClickSquare.bind(this), 740+posIncrement, 350);
            this.addChild(colorSqr); 
            posIncrement+=50;
        }

        

        posIncrement = 0;
        for (let index = 0; index < 4; index++) 
        {
            var colorSqr = new ColorSquare(0xFFFFFF, "white" , this.EmptyFunction.bind(this), 750, 110+posIncrement);
            this.addChild(colorSqr);
            posIncrement+=50;
            this.chosenColors.push(colorSqr);
        }

        var endButton = new EndTurnButton(this.OnClickEnd.bind(this));
        this.addChild(endButton);

        var textTurn = new Text("Turn: " + this.turn);
        textTurn.x = 930;
        textTurn.y = 500;

        this.addChild(textTurn);
    }

    EmptyFunction(){}

    OnClickSquare(colorName, colorCode)
    {
        var tempArry = [];
        this.chosenColors.forEach(element => 
        {
            tempArry.push(element.colorName);
        });

        if(tempArry.indexOf(colorName) === -1) 
        {
            var colorSqr = new ColorSquare(colorCode, colorName , this.EmptyFunction.bind(this), 750, 110);
            this.chosenColors.unshift(colorSqr);
            this.addChild(colorSqr);

            var posIncrement = 50;
            for (let index = 1; index <= 3; index++) 
            {
                var colorSqr = new ColorSquare(this.chosenColors[index].colorCode, this.chosenColors[index].colorName , this.EmptyFunction.bind(this), 750, 110+posIncrement);
                this.addChild(colorSqr);
                posIncrement+=50;
            }

            this.chosenColors.pop();
        }
    }

    OnClickEnd()
    {
        console.log(this.turn);
        var white=0;
        var black=0;
        var ex=0;

        var posIncrement = 0;
        for (let index = 0; index < this.chosenColors.length; index++) 
        {
            if(this.chosenColors[index].colorName == this.randomColors[index])
            {
                white++;
                var stuffDisplay = new UpdateStuffDisplay(1, 800+posIncrement, 110);
            }
            else if(this.randomColors.indexOf(this.chosenColors[index].colorName) != -1)
            {
                black++;
                var stuffDisplay = new UpdateStuffDisplay(2, 800+posIncrement, 110);
            }
            else
            {
                ex++;
                var stuffDisplay = new UpdateStuffDisplay(3, 800+posIncrement, 110);
                this.addChild(stuffDisplay);
            }
            posIncrement+=50;
        }

        console.log(white + " " + black + " " + ex);

        this.turn++;

        var textBg = new Graphics();
        textBg.beginFill(0x808080);
        textBg.drawRect(930,500,100,100);
        textBg.endFill();
        this.addChild(textBg);
        
        var textTurn = new Text("Turn: " + this.turn);
        textTurn.x = 930;
        textTurn.y = 500;
        this.addChild(textTurn);

        

        if(white == 4)
        {
            var win = new SpawnPicture(1);
            this.addChild(win);
        }
        else if(this.turn> 12)
        {
            var lose = new SpawnPicture(2);
            this.addChild(lose);
        }
    }

    GenerateRandomColors(colors)
    {
        while (this.randomColors.length != 4) 
        {
            var randValue = Math.floor( Math.random() * colors.length );
            if(this.randomColors.indexOf(colors[randValue]) === -1) 
            {
                this.randomColors.push(colors[randValue])
            };
        }

        //Displaying random colors for testing purposes.
        this.randomColors.forEach(element => 
        {
            console.log(element);    
        });
    }
}

export default TitleScreen;